# FTC Bus Monitor
This is monitoring tool on FTC bus. The FTC bus is standard protocol for Domotics product.
Physical hardware is 1 wire bus driving with 24 VDC.
We need FTC bus converter to transform 1-wire protocal to serial port (BAUD38400).

## Function
This software will show traffic of bus on web browser port 8081.

  * We can press **Address** button to enquire Domotics devices on the bus.
  * When we press **Version** button, all units will return thier own firmware version

